package com.cy.erp.common;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class BasePojo implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1524117079700962925L;
	private Date createdTime;
	private Date updatedTime;

}
