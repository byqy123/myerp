package com.cy.erp.common;

import java.util.Date;

import lombok.Data;

@Data
public class OrderHeader {
	
	private Long id;
	private Long organId;
	private String number;
	private String subType;
	private String type;
	private Date createdTime;
	private String remark;
	private Integer status;
	private Long totalPrice;
	private Long creator;
	private String salesMan;

}
