package com.cy.erp.common;

import java.util.List;

import lombok.Data;

@Data
public class EasyUITable<T> {
	
	private Integer total;
	private List<T> rows;
	public EasyUITable(Integer total, List<T> rows) {
		super();
		this.total = total;
		this.rows = rows;
	}

}
