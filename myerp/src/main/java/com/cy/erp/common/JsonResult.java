package com.cy.erp.common;

import java.io.Serializable;

/**
 * 封装服务端的响应数据
 * @author Administrator
 *
 */
public class JsonResult implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5415540679197546732L;
	//状态码
	private int state=1;
	//状态信息
	private String message="OK";
	//正常数据
	private Object data;
	
	public JsonResult() {
		
	}
	
	public JsonResult(String message) {

		this.message = message;
	}

	public JsonResult(Object data) {

		this.data = data;
	}
	public JsonResult(Throwable e) {
		this.state=0;
		this.message=e.getMessage();
	}
	
	
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	

}
