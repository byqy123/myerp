package com.cy.erp.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cy.erp.service.SupplierService;


@RestController
@RequestMapping("/supplier")
public class SupplierController {

	@Autowired
	SupplierService supplierService;
	
	@RequestMapping("/findAllnames")
	public List<Map<String,Object>> findAllNames(){
		return supplierService.findAllNames();
		
	}
	
	@RequestMapping("/findOrganNameById")
	public String findOrganNameById(Integer id) {
		return supplierService.findOrganNameById(id);
		
	}
}
