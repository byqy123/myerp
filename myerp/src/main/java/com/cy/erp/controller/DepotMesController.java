package com.cy.erp.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cy.erp.common.EasyUITable;
import com.cy.erp.datasource.entities.depot;
import com.cy.erp.service.DepotMesService;

@RestController
@RequestMapping(value = "/depotMes")
public class DepotMesController {
	
	@Autowired
	DepotMesService depotMesService;
	
	@RequestMapping("/findAllNames")
	public List<Map<String,Object>> findAllNames() {
		return depotMesService.findAllNames();
		
	}
	
	@RequestMapping("/findAllDepot")
	public List<Map<String,Object>> findAllDepot(){
		return depotMesService.findAllDepot();
	
	}
	
	@RequestMapping("/findDepotNameById")
	public String findDepotNameById(Integer id) {
		return depotMesService.findDepotNameById(id);
		
	}

}
