package com.cy.erp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cy.erp.datasource.entities.ItemCat;
import com.cy.erp.service.ItemCatService;

@RestController
@RequestMapping("/itemcat")
public class ItemCatController {
	
	@Autowired
	private ItemCatService itemCatService;
	
	@RequestMapping("/queryItemName")
	public String findItemCatNameById(Long itemCatId) {
		ItemCat itemCat=itemCatService.findItemCatNameById(itemCatId);
		return itemCat.getName();
	}
	
}
