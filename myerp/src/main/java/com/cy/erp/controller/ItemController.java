package com.cy.erp.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cy.erp.common.EasyUITable;
import com.cy.erp.common.JsonResult;
import com.cy.erp.datasource.entities.Item;
import com.cy.erp.service.ItemService;

@RestController
@RequestMapping(value = "/item")
public class ItemController {
	
	@Autowired
	private ItemService itemService;
	
	/**
	 * 查找分页记录
	 * @return
	 */
	@RequestMapping("/FindAllItem")
	public EasyUITable<Item> findItemByPage(Integer page,Integer rows) {

		return itemService.findItemByPage(page,rows);
		
	}
	
	@RequestMapping("/addItem")
	public JsonResult addItem(Item item) {
		
		itemService.insertItem(item);
		return new JsonResult("添加成功");
		
	}
	
	@RequestMapping("/selectById")
	public Item selectById(Integer id) {
		return itemService.selectById(id);
		
	}
	
	@RequestMapping("/updateItem")
	public JsonResult updateItem(Item item) {
		itemService.updateItemById(item);

		return new JsonResult("修改成功");
		
	}
	@RequestMapping("/findAllItemName")
	public List<Map<String,Object>> findAllItemName(){
		return itemService.findAllItemName();
	}
	
	@RequestMapping("/findItemNameById")
	public String findItemNameById(Integer id) {
		return itemService.findItemNameByid(id);
		
	}
	
	
}
