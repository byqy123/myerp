package com.cy.erp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cy.erp.common.JsonResult;
import com.cy.erp.service.SequenceService;

@RestController
@RequestMapping("/sequence")
public class SequenceController {
	
	@Autowired
	SequenceService sequenceService;
	
	@RequestMapping("/findValueById")
	public String findValueById(Integer id) {

		//JsonResult result = new JsonResult();
		String value= sequenceService.findValueById(id);
		//result.setData(value);
		return value;
		
	}
}
