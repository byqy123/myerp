package com.cy.erp.controller;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.cy.erp.common.EasyUITable;
import com.cy.erp.common.JsonResult;
import com.cy.erp.common.OrderHeader;
import com.cy.erp.datasource.entities.PurchaseOrderItem;
import com.cy.erp.service.PurchaseOrderService;
import com.fasterxml.jackson.annotation.JsonFormat;

@RestController
@RequestMapping("/PurchaseOrder")
public class PurchaseOrderController {

	@Autowired
	PurchaseOrderService purchaseOrderService;

	// 分页查询所有订单信息
	@RequestMapping("/findAllPages")
	public EasyUITable<OrderHeader> findAllPages(Integer page, Integer rows, String number, Long organId,
			@RequestParam("startTime") @DateTimeFormat(pattern = "yyyy-MM-dd") Date  startTime, String endTime) {
		 System.out.println("开始时间"+startTime);
		 System.out.println("结束时间"+endTime);
		 String sTime ="2021-09-15 15:15:32";
		return purchaseOrderService.findAllpages(page, rows, number, organId, sTime, endTime);

	}

	// 保存订单信息
	@RequestMapping("/doSaveOrderbject")
	public JsonResult doSaveOrderbject(OrderHeader header, String rows) {

		List<PurchaseOrderItem> list = JSON.parseArray(rows, PurchaseOrderItem.class);
		purchaseOrderService.saveOrderObject(header, list);
		return new JsonResult("保存成功");
	}

	// 根据订单头表id查询订单商品信息
	@RequestMapping("/findAllItemByheaderId")
	public List<PurchaseOrderItem> findAllItemByheaderId(Integer headerId) {

		List<PurchaseOrderItem> list = purchaseOrderService.findObjectByHeaderId(headerId);
		for (PurchaseOrderItem orderItem : list) {
			System.out.println(orderItem);
		}
		return list;
	}

	// 根据订单头表id查询订单商品名称
	@RequestMapping("/findItemNamesByHeaderId")
	public String findItemNamesByHeaderId(Integer id) {

		return purchaseOrderService.findItemNamesByHeaderId(id);
	}

	// 查询订单数据
	@RequestMapping("/findHeaderAndItemsByHeaderid")
	public Map<String, Object> findHeaderAndItemsByHeaderid(Integer id) {

		return purchaseOrderService.findHeaderAndItemsByHeaderid(id);
	}

	// 更新订单数据
	@RequestMapping("/doUpdateObject")
	public JsonResult doUpdateObject(OrderHeader header, String rows) {

		List<PurchaseOrderItem> list = JSON.parseArray(rows, PurchaseOrderItem.class);
		purchaseOrderService.updateOrderObject(header, list);
		return new JsonResult("修改成功！");

	}

	// 查询订单状态
	@RequestMapping("/findOrderStatus")
	public int findOrderStatus(Long id) {

		return purchaseOrderService.findOrderStatus(id);

	}

	@RequestMapping("/doTurnCheckOrder")
	public JsonResult doTurnCheckOrder(Long id) {

		return purchaseOrderService.turnCheckOrder(id);
	}

	@RequestMapping("/doCheckOrder")
	public JsonResult doCheckOrder(Long id) {

		return purchaseOrderService.checkOrder(id);
	}

	@RequestMapping("/doDeleteOrder")
	public JsonResult doDeleteOrder(Long id) {

		return purchaseOrderService.deleteOrder(id);
	}

}
