package com.cy.erp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class pagecontroller {
	
	@RequestMapping("{UI}")
	public String dopageui(@PathVariable String UI) {
		return UI;	
	}
	
	@RequestMapping("do")
	public String dotest() {
		return "index";
	}
	
	@RequestMapping("test/{module}")
	public String doModuleUI(@PathVariable String module) {
		return "pages/test/home2";
	}
	
	@RequestMapping("{module}/{UI}")
	public String doModule(@PathVariable String module,@PathVariable String UI) {
		return "pages/"+module+"/"+UI;
	}
}
