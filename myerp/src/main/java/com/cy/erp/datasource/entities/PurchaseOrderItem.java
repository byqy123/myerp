package com.cy.erp.datasource.entities;

import lombok.Data;

@Data
public class PurchaseOrderItem{

	private Long id;
	private Long headerId;
	private Long itemId;
	private Long depotId;
	private Long itemExtendId;
	private String color;
	private Integer length;
	private Integer width;
	private Integer leafHeight;
	private Integer footHeight;
	private String unit;
	private Integer number;
	private Long unitPrice;
	private Long taxUnitPrice;
	private Long allPrice;
	private Integer deleteFlag;

}
