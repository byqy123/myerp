package com.cy.erp.datasource.mappers;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface SupplierMapper {
	@Select("select id,name from supplier where type='供应商'")
	List<Map<String,Object>> findAllNames();

	@Select("select name from supplier where id=#{id}")
	String findOrganNameById(Integer id);
}
