package com.cy.erp.datasource.mappers;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.cy.erp.datasource.entities.ItemCat;

@Mapper
public interface ItemCatMapper {

	@Select("select name from item_cat where id=#{itemCatId}")
	ItemCat selectNameById(Long itemCatId);


}
