package com.cy.erp.datasource.mappers;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.cy.erp.datasource.entities.PurchaseOrderItem;


@Mapper
public interface PurchaseOrderItemMapper {

	int insertObject(PurchaseOrderItem order);
	
	List<PurchaseOrderItem> findObjectByHeaderId(Integer headerId);
	
	String[] findItemNamesByIds(Long[] ids);

	@Delete("delete from depot_item where header_id=#{id}")
	int deleteObjectByHeaderId(Long id);

	@Select("select status from depot_head where id=#{id}")
	int findOrderStatus(Long id);
	
	
	
}
