package com.cy.erp.datasource.mappers;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.cy.erp.datasource.entities.Item;


@Mapper
public interface ItemMapper {

	int getRowCount();
	
	List<Item> findPageObjects(
			@Param("startIndex")Integer startIndex, 
			@Param("pageSize")Integer pageSize);

	int insertItem(Item item);

	@Select("select * from item where id=#{id}")
	Item selectById(Integer id);

	int updateItemById(Item item);

	@Select("select id itemId,name itemName from item")
	List<Map<String, Object>> findAllItemName();

	@Select("select name from item where id=#{id}")
	String findItemNameById(Integer id);
}
