package com.cy.erp.datasource.entities;

import com.cy.erp.common.BasePojo;

import lombok.Data;

@Data
public class Item extends BasePojo{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 360843953709113152L;

	private Integer id;
	
	private String name;
	private Long price;
	private String unit;
	private String model;
	private String color;
	private Integer cid;
	private Integer status;


}
