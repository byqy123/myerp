package com.cy.erp.datasource.entities;

import java.util.Date;

import com.cy.erp.common.BasePojo;

import lombok.Data;

@Data
public class ItemCat extends BasePojo{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7904421506108421305L;
	private Long id;
	private Long parentId;
	private String name;
	private Integer status;
	private Integer sortOrder;
	private Boolean isParent;
	private Date createdTime;
	private Date updatedTime;	
}
