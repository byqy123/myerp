package com.cy.erp.datasource.mappers;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.cy.erp.datasource.entities.depot;

@Mapper
public interface DepotMesMapper {


	List<depot> findPageObject(int start, Integer rows);

	@Select("select count(*) from depot")
	int getRowcount();

	@Select("select * from depot")
	List<Map<String, Object>> findAllDepot();
	
	@Select("select id depotId,name depotName from depot")
	List<Map<String, Object>> findAllNames();

	@Select("select name from depot where id=#{id}")
	String findDepotNameById(Integer id);
	

}
