package com.cy.erp.datasource.mappers;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import com.cy.erp.common.OrderHeader;

@Mapper
public interface OrderHeaderMapper {
	
	@Select("select count(*) from depot_head")
	int getRowCount();
	
	List<OrderHeader> findAllPages(int start,Integer rows,String number,Long organId, Date STime, Date ETime);
	
	int insertOrderHeader(OrderHeader orderHeader);

	@Select("select number,createdTime,organId from depot_head where id=#{id}")
	OrderHeader findObjectById(Integer id);

	int updateOrderHeaderById(OrderHeader header);

	@Delete("delete from depot_head where id=#{id}")
	int deleteObjectById(Long id);

	int setStatusById(Long id, int status);


}
