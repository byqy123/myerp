package com.cy.erp.datasource.entities;

import lombok.Data;

@Data
public class depot {
	private Integer id;
	private String name;
	private String address;
	private Integer principal;
	private String remark;
	private Integer isDefault;
	
}
