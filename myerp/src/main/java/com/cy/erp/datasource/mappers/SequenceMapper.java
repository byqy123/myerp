package com.cy.erp.datasource.mappers;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

@Mapper
public interface SequenceMapper {
	
	@Update("update sequence set currentValue=currentValue+1 where id=#{id}")
	int addCurrentValueById(Integer id);

	@Select("select currentValue from sequence where id=#{id}")
	String findValueById(Integer id);
}
