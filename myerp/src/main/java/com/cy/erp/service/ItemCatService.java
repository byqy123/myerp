package com.cy.erp.service;

import com.cy.erp.datasource.entities.ItemCat;

public interface ItemCatService {

	ItemCat findItemCatNameById(Long itemCatId);

}
