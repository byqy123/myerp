package com.cy.erp.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.cy.erp.common.EasyUITable;
import com.cy.erp.common.JsonResult;
import com.cy.erp.common.OrderHeader;
import com.cy.erp.datasource.entities.PurchaseOrderItem;
import com.cy.erp.datasource.mappers.OrderHeaderMapper;
import com.cy.erp.datasource.mappers.PurchaseOrderItemMapper;
import com.cy.erp.datasource.mappers.SequenceMapper;
import com.cy.erp.service.PurchaseOrderService;

@Service
public class PurchaseOrderServiceimpl implements PurchaseOrderService {

	@Autowired
	OrderHeaderMapper orderHeaderMapper;

	@Autowired
	SequenceMapper sequenceMapper;

	@Autowired
	PurchaseOrderItemMapper purchaseOrderItemMapper;

	@Override
	public EasyUITable<OrderHeader> findAllpages(Integer page, Integer rows, String number, Long organId,
			String startTime, String endTime) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date STime = null;
		Date ETime = null;
		if (!StringUtils.isEmpty(startTime)) {
			try {
				STime = sdf.parse(startTime);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if (!StringUtils.isEmpty(endTime)) {
			try {
				ETime = sdf.parse(endTime);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		System.out.println("开始时间"+STime);
		System.out.println("结束时间"+ETime);
		int totle = orderHeaderMapper.getRowCount();
		int start = (page - 1) * rows;
		List<OrderHeader> list = orderHeaderMapper.findAllPages(start, rows, number, organId, STime, ETime);

		return new EasyUITable<>(totle, list);
	}

	@Override
	public void saveOrderObject(OrderHeader header, List<PurchaseOrderItem> list) {

		sequenceMapper.addCurrentValueById(1);

		header.setSubType("采购入库");
		header.setType("入库");
		header.setStatus(0);
		int headerRow = orderHeaderMapper.insertOrderHeader(header);
		System.out.println(headerRow);

		int rows = 0;
		for (PurchaseOrderItem order : list) {
			order.setHeaderId(header.getId());
			int row = purchaseOrderItemMapper.insertObject(order);
			rows = rows + row;
		}

	}

	@Override
	public List<PurchaseOrderItem> findObjectByHeaderId(Integer id) {

		return purchaseOrderItemMapper.findObjectByHeaderId(id);
	}

	@Override
	public String findItemNamesByHeaderId(Integer id) {
		Long[] ids = new Long[20];
		int i = 0;
		String items = "";
		List<PurchaseOrderItem> list = purchaseOrderItemMapper.findObjectByHeaderId(id);
		for (PurchaseOrderItem orderItem : list) {
			ids[i] = orderItem.getItemId();
			// System.out.println(ids[i]);
			i++;
		}
		String[] strlist = purchaseOrderItemMapper.findItemNamesByIds(ids);
		for (String str : strlist) {
			items += " ";
			items += str;
		}
		System.out.println(items);
		return items;
	}

	@Override
	public Map<String, Object> findHeaderAndItemsByHeaderid(Integer id) {

		OrderHeader orderHeader = orderHeaderMapper.findObjectById(id);
		List<PurchaseOrderItem> list = purchaseOrderItemMapper.findObjectByHeaderId(id);
		Map<String, Object> map = new HashMap<>();
		map.put("header", orderHeader);
		map.put("rowData", list);
		return map;
	}

	@Override
	public void updateOrderObject(OrderHeader header, List<PurchaseOrderItem> list) {

		// 更新订单表数据
		orderHeaderMapper.updateOrderHeaderById(header);
		// 删除订单详情表数据
		purchaseOrderItemMapper.deleteObjectByHeaderId(header.getId());
		int rows = 0;
		for (PurchaseOrderItem order : list) {
			order.setHeaderId(header.getId());
			int row = purchaseOrderItemMapper.insertObject(order);
			rows = rows + row;
		}

	}

	@Override
	public int findOrderStatus(Long id) {
		return purchaseOrderItemMapper.findOrderStatus(id);
	}

	@Override
	public JsonResult turnCheckOrder(Long id) {

		int status = 0;
		JsonResult result = new JsonResult();
		int row = orderHeaderMapper.setStatusById(id, status);
		if (row > 0) {
			result.setMessage("反审核成功");
			System.out.println("反审核成功");
		} else {
			result.setState(0);
			result.setMessage("反审核失败");
			System.out.println("反审核失败");
		}
		System.out.println(row);
		return result;
	}

	@Override
	public JsonResult checkOrder(Long id) {

		int status = 1;
		JsonResult result = new JsonResult();
		int row = orderHeaderMapper.setStatusById(id, status);
		if (row > 0) {
			result.setMessage("审核成功");
		} else {
			result.setState(0);
			result.setMessage("审核失败");
		}
		System.out.println(row);
		return result;
	}

	@Override
	public JsonResult deleteOrder(Long id) {

		JsonResult result = new JsonResult();
		purchaseOrderItemMapper.deleteObjectByHeaderId(id);
		int row = orderHeaderMapper.deleteObjectById(id);
		if (row > 0) {
			result.setMessage("删除成功");
		} else {
			result.setState(0);
			result.setMessage("删除失败");
		}
		return result;
	}

}
