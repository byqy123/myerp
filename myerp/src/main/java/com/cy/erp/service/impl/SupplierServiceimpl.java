package com.cy.erp.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.erp.datasource.mappers.SupplierMapper;
import com.cy.erp.service.SupplierService;

@Service
public class SupplierServiceimpl implements SupplierService {

	@Autowired
	SupplierMapper supplierMapper;
	@Override
	public List<Map<String, Object>> findAllNames() {
		// TODO Auto-generated method stub
		return supplierMapper.findAllNames();
	}
	@Override
	public String findOrganNameById(Integer id) {
		return supplierMapper.findOrganNameById(id);
	}

}
