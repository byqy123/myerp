package com.cy.erp.service;

import java.util.List;
import java.util.Map;

public interface SupplierService {
	
	List<Map<String,Object>> findAllNames();

	String findOrganNameById(Integer id);

}
