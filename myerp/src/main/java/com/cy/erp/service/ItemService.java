package com.cy.erp.service;

import java.util.List;
import java.util.Map;

import com.cy.erp.common.EasyUITable;
import com.cy.erp.datasource.entities.Item;

public interface ItemService {

	EasyUITable<Item> findItemByPage(Integer page, Integer rows);

	int insertItem(Item item);

	Item selectById(Integer id);

	int updateItemById(Item item);

	List<Map<String, Object>> findAllItemName();

	String findItemNameByid(Integer id);

}
