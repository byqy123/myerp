package com.cy.erp.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.erp.common.EasyUITable;
import com.cy.erp.datasource.entities.depot;
import com.cy.erp.datasource.mappers.DepotMesMapper;
import com.cy.erp.service.DepotMesService;

@Service
public class DepotMessServiceimpl implements DepotMesService {
	
	@Autowired
	DepotMesMapper depotMesMapper;

	@Override
	public EasyUITable<depot> findAllDepot(Integer page, Integer rows) {
		int totle=depotMesMapper.getRowcount();
		
		int start=(page-1)*rows;
		List<depot> list=depotMesMapper.findPageObject(start,rows);
		
		return new EasyUITable<>(totle, list);
	}

	@Override
	public List<Map<String, Object>> findAllNames() {

		return depotMesMapper.findAllNames();
	}
	
	@Override
	public List<Map<String, Object>> findAllDepot() {
		
		return depotMesMapper.findAllDepot();
	}

	@Override
	public String findDepotNameById(Integer id) {
		return depotMesMapper.findDepotNameById(id);
	}
}
