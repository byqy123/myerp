package com.cy.erp.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.erp.common.EasyUITable;
import com.cy.erp.datasource.entities.Item;
import com.cy.erp.datasource.mappers.ItemMapper;
import com.cy.erp.service.ItemService;


@Service
public class ItemServiceimpl implements ItemService {
	
	@Autowired
	private ItemMapper itemMapper;

	/**
	 * 分页查询
	 * 参数:page 当前页数  rows 条数
	 */
	@Override
	public EasyUITable<Item> findItemByPage(Integer page, Integer rows) {
		
		//获取记录总数
		int total=itemMapper.getRowCount();
		//分页查询
		int start=(page-1)*rows;
		List<Item> list=itemMapper.findPageObjects(start, rows);
		
		return new EasyUITable<>(total,list);
	}

	@Override
	public int insertItem(Item item) {
		item.setStatus(1);
		return itemMapper.insertItem(item);
	}

	@Override
	public Item selectById(Integer id) {
		Item item=itemMapper.selectById(id);
		return item;
	}

	@Override
	public int updateItemById(Item item) {
		
		return itemMapper.updateItemById(item);
	}

	@Override
	public List<Map<String, Object>> findAllItemName() {
		
		return itemMapper.findAllItemName();
	}

	@Override
	public String findItemNameByid(Integer id) {
		return itemMapper.findItemNameById(id);
	}

}
