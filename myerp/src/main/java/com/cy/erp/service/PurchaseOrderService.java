package com.cy.erp.service;

import java.util.List;
import java.util.Map;

import com.cy.erp.common.EasyUITable;
import com.cy.erp.common.JsonResult;
import com.cy.erp.common.OrderHeader;
import com.cy.erp.datasource.entities.PurchaseOrderItem;

public interface PurchaseOrderService {
	public EasyUITable<OrderHeader> findAllpages(Integer page,Integer rows, String number, Long organId, String startTime, String endTime);

	public void saveOrderObject(OrderHeader header, List<PurchaseOrderItem> list);
	
	public List<PurchaseOrderItem> findObjectByHeaderId(Integer id);

	public String findItemNamesByHeaderId(Integer id);

	public Map<String, Object> findHeaderAndItemsByHeaderid(Integer id);

	public void updateOrderObject(OrderHeader header, List<PurchaseOrderItem> list);

	public int findOrderStatus(Long id);

	public JsonResult turnCheckOrder(Long id);

	public JsonResult checkOrder(Long id);

	public JsonResult deleteOrder(Long id);
}
