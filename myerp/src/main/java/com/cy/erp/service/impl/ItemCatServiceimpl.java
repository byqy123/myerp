package com.cy.erp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.erp.datasource.entities.ItemCat;
import com.cy.erp.datasource.mappers.ItemCatMapper;
import com.cy.erp.service.ItemCatService;

@Service
public class ItemCatServiceimpl implements ItemCatService {

	@Autowired
	private ItemCatMapper itemCatMapper;

	@Override
	public ItemCat findItemCatNameById(Long itemCatId) {
		
		return itemCatMapper.selectNameById(itemCatId);
	}
}
