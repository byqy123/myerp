package com.cy.erp.service;

import java.util.List;
import java.util.Map;

import com.cy.erp.common.EasyUITable;
import com.cy.erp.datasource.entities.depot;

public interface DepotMesService {
	
	public EasyUITable<depot> findAllDepot(Integer page,Integer rows);

	public List<Map<String, Object>> findAllNames();
	public List<Map<String, Object>> findAllDepot();

	public String findDepotNameById(Integer id);

}
