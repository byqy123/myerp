package com.cy.erp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cy.erp.datasource.mappers.SequenceMapper;
import com.cy.erp.service.SequenceService;
@Service
public class SequenceServiceimpl implements SequenceService {
	@Autowired
	SequenceMapper sequenceMapper;

	@Override
	public String findValueById(Integer id) {

		return sequenceMapper.findValueById(id);
	}
	
	
}
